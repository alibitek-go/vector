# go vector

A [vector](https://en.wikipedia.org/wiki/Euclidean_vector) representation in Golang with 100% tests coverage.

## How to run tests?
`make run_tests`

## How to run tests with code coverage?
`make run_tests_with_coverage`

## How to run example client?
`make run_client`

## How to use in your code as an imported module?
1. Download the module: `go get gitlab.com/alibitek-go/vector`
2. Import the module: `import govector "gitlab.com/alibitek-go/vector"`
3. Use the module, see `example_client/vector_client.go` for a usage example.
