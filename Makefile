 .PHONY: run_tests run_tests_with_coverage run_client

GO=go
GOTEST=$(GO) test
GOCOVER=$(GO) tool cover
GORUN=$(GO) run

all: run_tests_with_coverage

run_tests:
	$(GOTEST) -v -race ./...
	
run_tests_with_coverage:
	$(GOTEST) -v -race -coverprofile=coverage.out ./...
	$(GOCOVER) -func=coverage.out
	$(GOCOVER) -html=coverage.out

run_client:
	$(GORUN) example_client/vector_client.go