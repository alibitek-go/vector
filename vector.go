package govector

import (
	"fmt"
	"math"
)

// Vector represents a three-dimensional vector: https://en.wikipedia.org/wiki/Euclidean_vector
// Methods on this struct have value receiver, thus they don't modify the original receiver.
type Vector struct {
	X, Y, Z float64
}

// Add adds two vectors
func (a Vector) Add(b Vector) Vector {
	return Vector{
		X: a.X + b.X,
		Y: a.Y + b.Y,
		Z: a.Z + b.Z,
	}
}

// Subtract subtracts two vectors
func (a Vector) Subtract(b Vector) Vector {
	return Vector{
		X: a.X - b.X,
		Y: a.Y - b.Y,
		Z: a.Z - b.Z,
	}
}

// MultiplyByScalar multiples each vector component by the provided scalar
func (a Vector) MultiplyByScalar(s float64) Vector {
	return Vector{
		X: a.X * s,
		Y: a.Y * s,
		Z: a.Z * s,
	}
}

// DivideByScalar divides each vector component by the provided scalar
func (a Vector) DivideByScalar(s float64) Vector {
	return a.MultiplyByScalar(1. / s)
}

// DotProduct computes the scalar product between two vectors
func (a Vector) DotProduct(b Vector) float64 {
	return (a.X * b.X) + (a.Y * b.Y) + (a.Z * b.Z)
}

// CrossProduct returns a vector perpendicular to vector a and b
func (a Vector) CrossProduct(b Vector) Vector {
	return Vector{
		X: a.Y*b.Z - a.Z*b.Y,
		Y: a.Z*b.X - a.X*b.Z,
		Z: a.X*b.Y - a.Y*b.X,
	}
}

// Length returns the length or magnitude of the vector.
func (a Vector) Length() float64 {
	return math.Sqrt(a.DotProduct(a))
}

// Normalize returns the unit vector, useful for indicating direction.
func (a Vector) Normalize() Vector {
	return a.DivideByScalar(a.Length())
}

// Equal determines if two vector are the same
func (a Vector) Equal(b Vector) bool {
	return a.X == b.X && a.Y == b.Y && a.Z == b.Z
}

// String implements the Stringer interface
func (a Vector) String() string {
	return fmt.Sprintf("Vector{X=%f, Y=%f, Z=%f}", a.X, a.Y, a.Z)
}
