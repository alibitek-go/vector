package main

import (
	"fmt"

	govector "gitlab.com/alibitek-go/vector"
)

func main() {
	v1 := govector.Vector{X: 1, Y: 2, Z: 3}
	v2 := govector.Vector{X: 9, Y: 8, Z: 7}
	fmt.Println(v1.Add(v2).String())
}
