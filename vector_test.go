package govector

import (
	"strconv"
	"testing"
)

func TestVector_Add(t *testing.T) {
	testCases := []struct {
		a        Vector
		b        Vector
		expected Vector
	}{
		{
			a:        Vector{1., 1., 1.},
			b:        Vector{2., 2., 2.},
			expected: Vector{3., 3., 3.},
		},
		{
			a:        Vector{1., 0., 0.},
			b:        Vector{0., 1., 0.},
			expected: Vector{1., 1., 0.},
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.Add(testCase.b)
			if !actual.Equal(testCase.expected) {
				t.Errorf("Adding the vectors failed! Expected: %s, Actual: %s", testCase.expected, actual)
			}
		})
	}
}

func TestVector_Subtract(t *testing.T) {
	testCases := []struct {
		a        Vector
		b        Vector
		expected Vector
	}{
		{
			a:        Vector{3., 3., 3.},
			b:        Vector{1., 1., 1.},
			expected: Vector{2., 2., 2.},
		},
		{
			a:        Vector{-1., 0., 0.},
			b:        Vector{1., 1., 0.},
			expected: Vector{-2., -1., 0.},
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.Subtract(testCase.b)
			if !actual.Equal(testCase.expected) {
				t.Errorf("Subtracting the vectors failed! Expected: %s, Actual: %s", testCase.expected, actual)
			}
		})
	}
}

func TestVector_MultiplyByScalar(t *testing.T) {
	testCases := []struct {
		a        Vector
		scalar   float64
		expected Vector
	}{
		{
			a:        Vector{3., 3., 3.},
			scalar:   3.,
			expected: Vector{9., 9., 9.},
		},
		{
			a:        Vector{2., 3., 4.},
			scalar:   2.,
			expected: Vector{4., 6., 8.},
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.MultiplyByScalar(testCase.scalar)
			if !actual.Equal(testCase.expected) {
				t.Errorf("MultiplyByScalar failed! Expected: %s, Actual: %s", testCase.expected, actual)
			}
		})
	}
}

func TestVector_DivideByScalar(t *testing.T) {
	testCases := []struct {
		a        Vector
		scalar   float64
		expected Vector
	}{
		{
			a:        Vector{3., 3., 3.},
			scalar:   3.,
			expected: Vector{1., 1., 1.},
		},
		{
			a:        Vector{2., 4., 6.},
			scalar:   2.,
			expected: Vector{1., 2., 3.},
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.DivideByScalar(testCase.scalar)
			if !actual.Equal(testCase.expected) {
				t.Errorf("DivideByScalar failed! Expected: %s, Actual: %s", testCase.expected, actual)
			}
		})
	}
}

func TestVector_DotProduct(t *testing.T) {
	testCases := []struct {
		a        Vector
		b        Vector
		expected float64
	}{
		{
			a:        Vector{1., 2., 3.},
			b:        Vector{-2, 0, 5},
			expected: 13,
		},
		{
			a:        Vector{1., 3., -5.},
			b:        Vector{4, -2, -1},
			expected: 3,
		},
		{ // perpendicular/orthogonal
			a:        Vector{1., 0., 0.},
			b:        Vector{0., 1., 0.},
			expected: 0,
		},
		{ // parallel/co-directional
			a:        Vector{1., 0., 0.},
			b:        Vector{1., 0., 0.},
			expected: 1,
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.DotProduct(testCase.b)
			if actual != testCase.expected {
				t.Errorf("DotProduct failed! Expected: %f, Actual: %f", testCase.expected, actual)
			}
		})
	}
}

func TestVector_Length(t *testing.T) {
	testCases := []struct {
		a        Vector
		expected float64
	}{
		{
			a:        Vector{5., 0., 0.},
			expected: 5,
		},
		{
			a:        Vector{0., 6., 0.},
			expected: 6,
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.Length()
			if actual != testCase.expected {
				t.Errorf("Length failed! Expected: %f, Actual: %f", testCase.expected, actual)
			}
		})
	}
}

func TestVector_CrossProduct(t *testing.T) {
	testCases := []struct {
		a        Vector
		b        Vector
		expected Vector
	}{
		{
			a:        Vector{1., 3., 4.},
			b:        Vector{2, 7, -5},
			expected: Vector{-43, 13, 1},
		},
		{
			a:        Vector{1., 0., 0.},
			b:        Vector{0., 1., 0.},
			expected: Vector{0., 0., 1.},
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.CrossProduct(testCase.b)
			if actual != testCase.expected {
				t.Errorf("CrossProduct failed! Expected: %f, Actual: %f", testCase.expected, actual)
			}
		})
	}
}

func TestVector_Normalize(t *testing.T) {
	testCases := []struct {
		a        Vector
		expected Vector
	}{
		{
			a:        Vector{0., 0., 10.},
			expected: Vector{0, 0, 1},
		},
		{
			a:        Vector{5., 0., 0.},
			expected: Vector{1., 0., 0.},
		},
		{
			a:        Vector{0., 0., 7.},
			expected: Vector{0., 0., 1.},
		},
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.Normalize()
			if actual != testCase.expected {
				t.Errorf("Normalize failed! Expected: %f, Actual: %f", testCase.expected, actual)
			}
		})
	}
}

func TestVector_String(t *testing.T) {
	testCases := []struct {
		a        Vector
		expected string
	}{
		{
			a:        Vector{0., 0., 10.},
			expected: "Vector{X=0.000000, Y=0.000000, Z=10.000000}",
		},
		{
			a:        Vector{5.2, 72.556, 0.29},
			expected: "Vector{X=5.200000, Y=72.556000, Z=0.290000}",
		},
	}

	v := Vector{}
	actualString := v.String()
	expectedString := "Vector{X=0.000000, Y=0.000000, Z=0.000000}"
	if actualString != expectedString {
		t.Errorf("Turning vector into string failed! Expected: %s, Actual: %s", expectedString, actualString)
	}

	for testCaseID, testCase := range testCases {
		t.Run(strconv.Itoa(testCaseID), func(t *testing.T) {
			actual := testCase.a.String()
			if actual != testCase.expected {
				t.Errorf("String failed! Expected: %s, Actual: %s", testCase.expected, actual)
			}
		})
	}
}
